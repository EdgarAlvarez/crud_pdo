<?php

class Database{
     
    private $host       = "localhost";
    private $db_name    = "framework";
    private $username   = "root";
    private $password   = "";
    public $conn;
    
 
    public function dbConnection(){
     
        $this->conn = null;    
        try{
            
            //OJO falta definir caracteres UTF-8
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
        
        }catch(PDOException $exception){
            echo "Error de conexión a la base de datos: " . $exception->getMessage();
        }
         
        return $this->conn;
    }
}