<?php
// CONTROLADOR USUARIOS

session_start();
include '../Model/crud.php';
$db = new crud();

$tblName = 'users';

if(isset($_REQUEST['action_type']) && !empty($_REQUEST['action_type'])){
    
    //Insertar
    if($_REQUEST['action_type'] == 'add'){
        $userData = array(
            'name'  => $_POST['name'],
            'email' => $_POST['email'],
            'phone' => $_POST['phone']
        );
        $insert     = $db->insert($tblName,$userData);
        $statusMsg  = $insert?'Se insertó correctamente.':'Ocurrió un problema, intentelo nuevamente.';
        $_SESSION['statusMsg'] = $statusMsg;
        header("Location:../index.php");
    

    //Editar
    }elseif($_REQUEST['action_type'] == 'edit'){
        if(!empty($_POST['id'])){
            $userData = array(
                'name'  => $_POST['name'],
                'email' => $_POST['email'],
                'phone' => $_POST['phone']
            );
            $condition  = array('id' => $_POST['id']);
            $update     = $db->update($tblName,$userData,$condition);
            $statusMsg  = $update?'Actualizaste correctamente los datos.':'Ocurrió un problema, intentelo nuevamente';
            $_SESSION['statusMsg'] = $statusMsg;
            header("Location:../index.php");
        }
    

    //Eliminar
    }elseif($_REQUEST['action_type'] == 'delete'){
        if(!empty($_GET['id'])){
            $condition  = array('id' => $_GET['id']);
            $delete     = $db->delete($tblName,$condition);
            $statusMsg  = $delete?'Se eliminó la información del usuario.':'Ocurrió un problema, intentelo nuevamente.';
            $_SESSION['statusMsg'] = $statusMsg;
            header("Location:../index.php");
        }
    }
}